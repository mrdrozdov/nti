"""
A naive Theano implementation of a stack whose elements are symbolic vector
values. This "fat stack" powers the "fat classifier" model, which supports
training and inference in all model configurations.

Of course, we sacrifice speed for this flexibility. Theano's symbolic
differentiation algorithm isn't friendly to this implementation, and
so it suffers from poor backpropagation runtime efficiency. It is also
relatively slow to compile.
"""

from functools import partial

import numpy as np

# Chainer imports
import chainer
from chainer import cuda, Function, gradient_check, report, training, utils, Variable
from chainer import datasets, iterators, optimizers, serializers
from chainer import Link, Chain, ChainList
import chainer.functions as F
from chainer.functions.connection import embed_id
from chainer.functions.normalization.batch_normalization import batch_normalization
from chainer.functions.evaluation import accuracy
import chainer.links as L
from chainer.training import extensions

from chainer.functions.activation import slstm
from chainer.utils import type_check

"""
Documentation Symbols:

B: Batch Size
B*: Dynamic Batch Size
S: Sequence Length
S*: Dynamic Sequence Length
E: Embedding Size
H: Output Size of Current Module

Style Guide:

1. Each __call__() or forward() should be documented with its
   input and output types/dimensions.
2. Every ChainList/Chain/Link needs to have assigned a __gpu and __mod.
3. Each __call__() or forward() should have `train` as a parameter,
   and Variables need to be set to Volatile=True during evaluation.
4. Each __call__() or forward() should have an accompanying `check_type_forward`
   called along the lines of:

   ```
   in_data = tuple([x.data for x in [input_1, input_2]])
   in_types = type_check.get_types(in_data, 'in_types', False)
   self.check_type_forward(in_types)
   ```

   This is mimicing the behavior seen in Chainer Functions.
5. Each __call__() or forward() should have a chainer.Variable as input.
   There may be slight exceptions to this rule, since at a times
   especially in this model a list is preferred, but try to stick to
   this as close as possible.

TODO:

- [x] Compute embeddings for initial sequences.
- [x] Convert embeddings into list of lists of Chainer Variables.
- [x] Loop over transitions, modifying buffer and stack as
      necessary using ``PseudoReduce''.
      NOTE: In this implementation, we pad the transitions
      with `-1` to indicate ``skip''.
- [x] Add projection layer to convert embeddings into proper
      dimensions for the TreeLSTM.
- [x] Use TreeLSTM reduce in place of PseudoReduce.
- [x] Debug NoneType that is coming out of gradient. You probably
      have to pad the sentences. SOLVED: The gradient was not
      being generated for the projection layer because of a
      redundant called to Variable().
- [x] Use the right C and H units for the TreeLSTM.
- [x] Enable evaluation. Currently crashing.
- [ ] Confirm that volatile is working correctly during eval time.
      Time the eval with and without volatile being set. Full eval
      takes about 2m to complete on AD Mac.

Other Tasks:

- [x] Run CBOW. 
- [ ] Enable Cropping and use longer sequences. Currently will
      not work as expected.
- [ ] Enable "transition validation".
- [ ] Enable TreeGRU as alternative option to TreeLSTM.
- [ ] Add TrackingLSTM.
- [ ] Run RNN for comparison.

Questions:

- [ ] Is the Projection layer implemented correctly? Efficiently?
- [ ] Is the composition with TreeLSTM implemented correctly? Efficiently?
- [ ] What should the types of Transitions and Y labels be? np.int64?

"""


class TreeLSTMChain(Chain):
    def __init__(self, hidden_dim, prefix="TreeLSTMChain", gpu=-1):
        super(TreeLSTMChain, self).__init__(
            W_l=L.Linear(hidden_dim, hidden_dim*5, nobias=True),
            W_r=L.Linear(hidden_dim, hidden_dim*5, nobias=True),
            b=L.Bias(axis=1, shape=(hidden_dim*5,)),
            )
        self.hidden_dim = hidden_dim
        self.__gpu = gpu
        self.__mod = cuda.cupy if gpu >= 0 else np

    def __call__(self, l_prev, r_prev, train=True, keep_hs=False):
        hidden_dim = self.hidden_dim

        l_h_prev = l_prev[:, :hidden_dim]
        l_c_prev = l_prev[:,  hidden_dim:]
        r_h_prev = r_prev[:, :hidden_dim]
        r_c_prev = r_prev[:,  hidden_dim:]

        gates = self.b(self.W_l(l_h_prev) + self.W_r(r_h_prev))

        def slice_gate(gate_data, i):
            return gate_data[:, i * hidden_dim:(i + 1) * hidden_dim]

        # Compute and slice gate values
        i_gate, fl_gate, fr_gate, o_gate, cell_inp = \
            [slice_gate(gates, i) for i in range(5)]

        # Apply nonlinearities
        i_gate = F.sigmoid(i_gate)
        fl_gate = F.sigmoid(fl_gate)
        fr_gate = F.sigmoid(fr_gate)
        o_gate = F.sigmoid(o_gate)
        cell_inp = F.tanh(cell_inp)

        # Compute new cell and hidden value
        c_t = fl_gate * l_c_prev + fr_gate * r_c_prev + i_gate * cell_inp
        h_t = o_gate * F.tanh(c_t)

        return F.concat([h_t, c_t], axis=1)


class ReduceChain(Chain):
    def __init__(self, hidden_dim, prefix="ReduceChain", gpu=-1):
        super(ReduceChain, self).__init__(
            treelstm=TreeLSTMChain(hidden_dim / 2),
        )
        self.hidden_dim = hidden_dim
        self.__gpu = gpu
        self.__mod = cuda.cupy if gpu >= 0 else np

    def check_type_forward(self, in_types):
        type_check.expect(in_types.size() == 2)
        left_type, right_type = in_types

        type_check.expect(
            left_type.dtype == 'f',
            left_type.ndim >= 1,
            right_type.dtype == 'f',
            right_type.ndim >= 1,
        )

    def __call__(self, left_x, right_x, train=True, keep_hs=False):
        """
        Args:
            left_x:  B* x H
            right_x: B* x H
        Returns:
            final_state: B* x H
        """

        # BEGIN: Type Check
        for l, r in zip(left_x, right_x):
            in_data = tuple([x.data for x in [l, r]])
            in_types = type_check.get_types(in_data, 'in_types', False)
            self.check_type_forward(in_types)
        # END: Type Check

        assert len(left_x) == len(right_x)
        batch_size = len(left_x)

        # Concatenate the list of states.
        left_x = F.concat(left_x, axis=0)
        right_x = F.concat(right_x, axis=0)
        assert left_x.shape == right_x.shape, "Left and Right must match in dimensions."
        
        assert left_x.shape[1] % 2 == 0, "Unit dim needs to be even because is concatenated [c,h]"
        unit_dim = left_x.shape[1]
        h_dim = unit_dim / 2

        # Split each state into its c/h representations.
        lstm_state = self.treelstm(left_x, right_x)
        return lstm_state


class SPINN(Chain):
    def __init__(self, hidden_dim, keep_rate, prefix="SPINN", gpu=-1):
        super(SPINN, self).__init__(
            reduce=ReduceChain(hidden_dim, gpu=gpu),
        )
        self.hidden_dim = hidden_dim
        self.__gpu = gpu
        self.__mod = cuda.cupy if gpu >= 0 else np

    def __call__(self, buffers, transitions, train=True, keep_hs=False, use_sum=False):
        """
        Pass over batches of transitions, modifying their associated
        buffers at each iteration.

        Args:
            buffers: List of B x S* x E
            transitions: List of B x S
        Returns:
            final_state: List of B x E
        """

        # TODO: There is no reason to pad sentences!

        batch_size, hidden_dim = len(buffers), 300
        # transitions = zip()

        buffers = [[Variable(w, volatile=not train) for w in s] for s in buffers]
        buffers_t = [0 for _ in buffers]
        transitions_t = [0 for _ in buffers]

        # Initialize stack with at least one item, otherwise gradient might
        # not propogate.
        stacks = [[] for b in buffers]

        def pseudo_reduce(lefts, rights):
            for l, r in zip(lefts, rights):
                yield l + r

        def better_reduce(lefts, rights):
            lstm_state = self.reduce(lefts, rights, train=train)
            batch_size = lstm_state.shape[0]
            lstm_state = F.split_axis(lstm_state, batch_size, axis=0, force_tuple=True)
            for state in lstm_state:
                yield state

        while sum(transitions_t) != batch_size * -1:
            lefts = []
            rights = []
            for i, (buf, stack) in enumerate(zip(buffers, stacks)):
                while transitions_t[i] < len(transitions[i]) and transitions_t[i] != -1:
                    t = transitions[i][transitions_t[i]]
                    if t == -1: # skip
                        # Because sentences are padded, we still need to pop here.
                        assert buffers_t[i] < len(buf)
                        buffers_t[i] += 1
                    elif t == 0: # shift
                        assert buffers_t[i] < len(buf)
                        new_stack_item = buf[buffers_t[i]]
                        assert new_stack_item.shape[0] == 1
                        assert isinstance(new_stack_item.data, np.ndarray), 'Wrong type: {}'.format(type(new_stack_item.data))
                        stack.append(new_stack_item)
                        buffers_t[i] += 1
                    elif t == 1: # reduce
                        for lr in [rights, lefts]:
                            if len(stack) > 0:
                                lr.append(stack.pop())
                            else:
                                # NOTE!!! This seems to only trigger during eval???
                                lr.append(Variable(
                                    np.zeros((1, hidden_dim), dtype=np.float32),
                                    volatile=not train))
                    else:
                        raise Exception("Action not implemented: {}".format(t))

                    # Keep looping if not reducing.
                    transitions_t[i] += 1
                    if t == 1:
                        break

            assert len(lefts) == len(rights)
            if len(rights) > 0:
                reduced = iter(better_reduce(lefts, rights))
                for i, (buf, stack) in enumerate(zip(buffers, stacks)):
                    if transitions_t[i] == -1:
                        continue
                    ts = transitions[i]
                    t = ts[transitions_t[i] - 1]
                    if t == -1 or t == 0:
                        continue
                    elif t == 1:
                        composition = next(reduced)
                        assert composition.shape[0] == 1
                        assert isinstance(composition.data, np.ndarray), 'Wrong type: {}'.format(type(composition.data))
                        stack.append(composition)
                    else:
                        raise Exception("Action not implemented: {}".format(t))

            for i in range(len(transitions_t)):
                if transitions_t[i] >= len(transitions[i]):
                    transitions_t[i] = -1

        ret = F.concat([s.pop() for s in stacks], axis=0)
        assert ret.shape == (batch_size, hidden_dim)

        return ret


class SentencePairModel(Chain):
    def __init__(self, model_dim, gpu, word_embedding_dim=300, keep_rate=0.9):
        super(SentencePairModel, self).__init__(
            # projection=L.Linear(word_embedding_dim, model_dim, nobias=True),
            x2h=SPINN(model_dim, gpu=gpu, keep_rate=keep_rate),
            h_l1 = F.Linear(2*model_dim, 1024),
            l_y = F.Linear(1024, 3),
            # batch_norm_0=L.BatchNormalization(model_dim*2, model_dim*2),
            # batch_norm_1=L.BatchNormalization(mlp_dim, mlp_dim),
            # batch_norm_2=L.BatchNormalization(mlp_dim, mlp_dim),
            # l0=L.Linear(model_dim*2, mlp_dim),
            # l1=L.Linear(mlp_dim, mlp_dim),
            # l2=L.Linear(mlp_dim, 3)
        )
        self.__gpu = gpu
        self.__mod = cuda.cupy if gpu >= 0 else np
        self.accFun = accuracy.accuracy
        self.keep_rate = 0.9
        self.word_embedding_dim = word_embedding_dim
        self.model_dim = model_dim

    def __forward(self, train, sentences, y_batch=None, transitions=None):
        ratio = 1 - self.keep_rate

        assert transitions is not None
        # import ipdb; ipdb.set_trace()

        batch_size = len(sentences)

        # Pass through Sentence Encoders.
        h_both = self.x2h(sentences, transitions, train=train)
        h_premise, h_hypothesis = F.split_axis(h_both, 2, axis=0)
        
        # Pass through MLP Classifier.
        # h = F.concat([h_premise, h_hypothesis], axis=1)
        # h = self.batch_norm_0(h, test=not train)
        # h = F.dropout(h, ratio, train)
        # h = F.relu(h)
        # h = self.l0(h)
        # h = self.batch_norm_1(h, test=not train)
        # h = F.dropout(h, ratio, train)
        # h = F.relu(h)
        # h = self.l1(h)
        # h = self.batch_norm_2(h, test=not train)
        # h = F.dropout(h, ratio, train)
        # h = F.relu(h)
        # h = self.l2(h)
        # y = h

        hs = F.relu(self.h_l1(F.concat([h_premise, h_hypothesis], axis=1)))
        y = self.l_y(F.dropout(hs, ratio=0.2, train=train))
        preds = self.__mod.argmax(y.data, 1).tolist()

        accum_loss = 0 if train else None
        if train:
            if self.__gpu >= 0:
                y_batch = cuda.to_gpu(y_batch)
            lbl = Variable(y_batch, volatile=not train)
            accum_loss = F.softmax_cross_entropy(y, lbl)

        return preds, accum_loss

    def init_optimizer(self):
        self.__opt = optimizers.Adam(alpha=0.0003, beta1=0.9, beta2=0.999, eps=1e-08)
        self.__opt.setup(self)
        self.__opt.add_hook(chainer.optimizer.GradientClipping(40))
        self.__opt.add_hook(chainer.optimizer.WeightDecay(0.00003))

    def train(self, x_batch, y_batch, ts_batch):
        self.__opt.zero_grads()
        preds, accum_loss = self.__forward(True, x_batch, y_batch=y_batch, transitions=ts_batch)
        accum_loss.backward()
        self.__opt.update()
        return preds, accum_loss

    def predict(self, x_batch, ts_batch):
        return self.__forward(False, x_batch, y_batch=None, transitions=ts_batch)[0]
