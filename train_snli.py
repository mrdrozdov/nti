import math
import sys
import time
import copy
import pandas as pd
import numpy as np
import six
from collections import Counter
import gc
import argparse

import chainer
from chainer import cuda, Variable, FunctionSet, optimizers
import chainer.functions  as F

from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.preprocessing import LabelEncoder
from sklearn.externals import joblib

from Preprocessing import preprocess4, batch, convert_binary_bracketing

from models import NTIFullTreeMatching
from models.spinn import SentencePairModel as SPINN

print "chainer version:", chainer.__version__

parser = argparse.ArgumentParser()
parser.add_argument('--gpu', '-g', type=int, default=0,
					help='GPU ID (negative value indicates CPU)')
parser.add_argument('--snli', '-i', default='path/to/snli_1.0',
					help='Directory to read SNLI data')
parser.add_argument('--glove', '-w', default='path/to/glove.840B.300d.txt',
					help='File to read glove vectors')
parser.add_argument('--out', '-o', default='result',
					help='Directory to output the trained models')
parser.add_argument('--test', '-t', type=bool, default=False,
					help='Use tiny datasets for quick tests')

args = parser.parse_args()

gpu = args.gpu
input_dir = args.snli
glove_path = args.glove
out_dir = args.out
test_run = args.test

n_epoch   = 25   # number of epochs
n_units   = 300  # number of units per layer
batch_size = 32  # minibatch size
eval_batch = 64
seq_length = 32
test_run_limit = 1000
stats_interval = 100

begin = time.time()
split = begin

EMPTY = np.random.uniform(-0.1, 0.1, (1, 300)).astype(np.float32)
joblib.dump(EMPTY, out_dir + '/NTIFullTreeMatching.empty')

def lap(text):
	global split
	local_split = time.time()
	diff = local_split - split 
	print("[{:10}] {}".format(diff, text))
	split = local_split

def get_vec(word):
	try:
		vec = vectors[word]
	except:
		vec = np.zeros((1, 300), dtype=np.float32)
	return vec

def word2vec(sent, crop_and_pad=False):
	words = sent
	if crop_and_pad:
		words = words[:seq_length]
		words = ['<EMPTY>'] * (seq_length - len(words)) + words
	return [get_vec(word) for word in words]

def parse_transitions(snli):
	lap ("Parsing transitions...")
	cols = []
	for i in xrange(len(snli.gold_label)):
		try:
			s1, t1 = convert_binary_bracketing(snli.sentence1_binary_parse[i])
			s2, t2 = convert_binary_bracketing(snli.sentence1_binary_parse[i])
			if i > test_run_limit and test_run:
				cols.append((None, None, None, None))
			else:
				cols.append((s1, t1, s2, t2))
		except Exception, e:
			print "got exception in line ", i
			cols.append((None, None, None, None))
	lap ("Delete old cols...")
	del snli['sentence1_binary_parse']
	del snli['sentence2_binary_parse']
	lap ("Setting new cols...")
	snli['sentence1'], snli['transitions1'], snli['sentence2'], snli['transitions2'] = zip(*cols)
lap ("Loading data...")
snli_dev = pd.read_csv(input_dir + '/snli_1.0_dev.txt', sep='\t', usecols=['gold_label', 'sentence1_binary_parse', 'sentence2_binary_parse'])
snli_train = pd.read_csv(input_dir + '/snli_1.0_train.txt', sep='\t', usecols=['gold_label', 'sentence1_binary_parse', 'sentence2_binary_parse'])
snli_test = pd.read_csv(input_dir + '/snli_1.0_test.txt', sep='\t', usecols=['gold_label', 'sentence1_binary_parse', 'sentence2_binary_parse'])

if test_run:
	snli_dev.drop(snli_dev.index[range(test_run_limit,len(snli_dev))], inplace=True)
	snli_train.drop(snli_train.index[range(test_run_limit,len(snli_train))], inplace=True)
	snli_test.drop(snli_test.index[range(test_run_limit,len(snli_test))], inplace=True)

print(snli_dev.shape)
print(snli_train.shape)
print(snli_test.shape)

parse_transitions(snli_dev)
parse_transitions(snli_train)
parse_transitions(snli_test)
def all_vocab(snli):
	vocab = set()
	for i in xrange(len(snli.gold_label)):
		try:
			if i > test_run_limit and test_run:
				break
			sent1 = snli.sentence1[i]
			sent2 = snli.sentence2[i]
			if sent1 == None:
				continue
			vocab.update(sent1)
			vocab.update(sent2)
		except Exception, e:
			import ipdb; ipdb.set_trace()
			print "got exception in line ", i
	return vocab

lap ("Building vocab...")
vocab = all_vocab(snli_dev) | all_vocab(snli_train) | all_vocab(snli_test)
with open(glove_path, 'r') as f:
		vectors = {}
		for line in f:
			vals = line.rstrip().split(' ')
			if vals[0] in vocab:
				vectors[vals[0]] =  np.array(vals[1:], ndmin=2, dtype=np.float32)
		vectors['<EMPTY>'] = EMPTY
vocab = None
gc.collect()

lap ("Preprocessing...")
def preprocess_set(snli):
	lbls = []
	ts = []
	ds = []
	s = 0
	for i in xrange(len(snli.gold_label)):
		try:
			if i > test_run_limit and test_run:
				break
			lbl = snli.gold_label[i]
			sent1 = snli.sentence1[i]
			sent2 = snli.sentence2[i]

			if len(sent1) >= seq_length/2 - 1 or len(sent1) >= seq_length/2 - 1:
				continue

			if lbl != '-':
				sents = [word2vec(sent) for sent in [sent1, sent2]]
				_ts = [snli.transitions1[i], snli.transitions2[i]]
				ts.append(_ts)
				ds.append(sents)
				lbls.append(lbl)
		except Exception, e:
			import ipdb; ipdb.set_trace()
			print "got exception in line ", i
			print e
			s+=1
	print "docs can't preprocess:", s
	return ds, lbls, ts

def stack_pairs(sent_batch):
	sents1 = []
	sents2 = []
	for sent1, sent2 in sent_batch:
		sents1.append(sent1)
		sents2.append(sent2)
	return sents1 + sents2

snli_train, lbls_tr, ts_tr = preprocess_set(snli_train)
snli_dev, lbls_dev, ts_dev = preprocess_set(snli_dev)
snli_test, lbls_test, ts_test = preprocess_set(snli_test)
vectors = None
gc.collect()
le = LabelEncoder()
le.fit(lbls_tr)
n_outputs = le.classes_.shape[0]
lbls_tr = le.transform(lbls_tr)
lbls_dev = le.transform(lbls_dev)
lbls_test = le.transform(lbls_test)

print "Train size:", len(snli_train)
print "Output size:", n_outputs

n_train = len(snli_train)
n_dev = len(snli_dev)
n_test = len(snli_test)

print "batch_size", batch_size
print "GPU", gpu

maxes = []
lap("Building model...")
# model = NTIFullTreeMatching(n_units, gpu)
model = SPINN(n_units, gpu)
print "model:",model
model.init_optimizer()
max_epch = 0
max_tr = 0
max_dev = 0
max_test = 0
lap("Train looping...")
from tqdm import tqdm
for i in xrange(0, n_epoch):
	print "epoch={}".format(i)
	#Shuffle the data
	shuffle = np.random.permutation(n_train)
	preds=[]
	preds_true=[]
	aLoss = 0
	ss = 0
	begin_time = time.time()
	for step, j in enumerate(tqdm(six.moves.range(0, n_train, batch_size))):
		c_b = shuffle[j:min(j+batch_size, n_train)]
		ys = batch(lbls_tr, c_b)
		ts_batch = batch(ts_tr, c_b)
		ts_batch = stack_pairs(ts_batch)
		preds_true.extend(ys)
		y_data = np.array(ys, dtype=np.int32)
		sent_batch = batch(snli_train, c_b)
		sent_batch = stack_pairs(sent_batch)
		y_s, loss = model.train(sent_batch, y_data, ts_batch)
		aLoss = aLoss + loss.data
		preds.extend(y_s)
		ss = ss + 1
		if step % stats_interval == 0:
			f1_tr = accuracy_score(preds_true, preds)
			print "[{},{}] loss: {} acc: {}".format(i, j, aLoss/ss, f1_tr)
	print "loss:", aLoss/ss
	print 'secs per train epoch={}'.format(time.time() - begin_time)
	f1_tr = accuracy_score(preds_true, preds)
	print 'train accuracy_score={}'.format(f1_tr)
	# print confusion_matrix(preds_true, preds)
	preds = []
	preds_true=[]
	for j in six.moves.range(0, n_dev, eval_batch):
		ys = lbls_dev[j:j+eval_batch]
		preds_true.extend(ys)
		y_data = np.array(ys, dtype=np.int32)
		sent_batch =  snli_dev[j:j+eval_batch]
		sent_batch = stack_pairs(sent_batch)
		ts_batch =  ts_dev[j:j+eval_batch]
		ts_batch = stack_pairs(ts_batch)
		y_s = model.predict(sent_batch, ts_batch)
		preds.extend(y_s)
	f1_dev = accuracy_score(preds_true, preds)
	print 'dev accuracy_score={}'.format(f1_dev)
	# print confusion_matrix(preds_true, preds)

	TESTING = False
	if TESTING:
		if f1_dev > max_dev:
			preds = []
			for j in six.moves.range(0, n_test, eval_batch):
				sent_batch = snli_test[j:j+eval_batch]
				sent_batch = stack_pairs(sent_batch)
				ts_batch =  ts_test[j:j+eval_batch]
				ts_batch = stack_pairs(ts_batch)
				y_s = model.predict(sent_batch)
				preds.extend(y_s)
			f1_test = accuracy_score(lbls_test, preds)
			print 'test accuracy_score={}'.format(f1_test)
			# print confusion_matrix(lbls_test, preds)
			max_dev = f1_dev
			max_test = f1_test
			max_tr = f1_tr
			max_epch = i
			print 'saving model...'
			model.save(out_dir + '/NTIFullTreeMatching.' + str(i))
			# print 'loading back model...'
			# loadModel = NTIFullTreeMatching.load(out_dir + '/NTIFullTreeMatching.' + str(i), n_units, gpu)
	print "best results so far (dev):"
	print "epoch=",max_epch
	print "dev f1-score=",max_dev
	print "test f1-score=",max_test
	if i - max_epch > 5:
		print "No recent improvement on dev, early stopping..."
		break